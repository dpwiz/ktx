# Changelog for ktx-codec

## [0.0.2.1] - 2023-04-08

* Add `Codec.Ktx2.Write`.
* Add patterns for Khronos keys
* Add patterns for Khronos DFD.BasicV2

[0.0.2.1]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.2.1-codec

## [0.0.2.0] - 2023-03-18

* Add experimental KTX 2.0 support.
  * Only reading for now.
  * Everything besides the header is read on demand.
  * MipLevel data can be read into provided buffer directly.
* `KeyValueData` moved to `Codec.Ktx.KeyValue` and dropped the `Key` newtype.
  * Add `textual` to extract all valid text values.

[0.0.2.0]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.2.0-codec

## [0.0.1.4] - 2022-01-24

* Put tests under a flag. No code change.
* Allow `text-2`.

[0.0.1.4]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.1.4-codec

## [0.0.1.3] - 2021-04-04

* Allow bytestring-0.12.
* Fix `getKeyValueData` decoder.
* Fix encoder endianness.
* Add `fromByteStringLazy`, `toBuilder` and `toFile`.

[0.0.1.3]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.1.3-codec

## [0.0.1.2] - 2021-01-18

* Add `fromByteString`.

[0.0.1.2]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.1.2-codec

## [0.0.1.1] - 2020-08-24

* Debug traces removed.

[0.0.1.1]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.1.1-codec

## [0.0.1.0] - 2020-08-23

* Initial release.

[0.0.1.0]: https://gitlab.com/dpwiz/ktx/-/tree/v0.0.1.0-codec
